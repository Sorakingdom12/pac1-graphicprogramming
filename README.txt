PARATROOPER GAME

///////////////////////////////////////////////////////////////////
			1. Workflow
///////////////////////////////////////////////////////////////////

Dado el proyecto autogenerado por Raylib, he procurado cambiar la 
detección de las teclas para facilitar el salto entre escenas tal y 
como se pide en el enunciado. 

///////////////////////////////////////////////////////////////////
			2. Código
///////////////////////////////////////////////////////////////////

Como habitualmente trabajo en unity, he decidido crear una estructura
similar que facilite las cosas. He creado una clase llamada AssetManager
que se encarga de servir los assets como sonidos o texturas al resto del
codigo dado que es un singleton y se puede acceder a su instancia.

A partir de ahí he añadido las clases EnemyManager y DeadManager
que son creadas en el gameplayScreen para que controlen toda la lógica
de los enemigos. El EnemyManager se hace cargo de hacer update y draw de
cada uno de los helicopteros y soldados que estan vivos. Mientras que el
DeadManager hace lo mismo para los muertos.
El EnemyManager además se hace cargo de controlar si un helicoptero tiene
el timer de spawn de soldados a zero y realiza las operaciones de reset y
spawn para crearlos.

Una vez aclaradas estas dos, el GameplayScreen se encarga de realizar las
funciones de update de ambas y las de draw creando así una estructura de 
árbol en la que la gameplay screen controla todos los objetos de la escena.
De esta forma también se recogen las entradas de las teclas 'a', 'd' y 'space'
que mueven la torreta aumentando o disminuyendo el angulo de rotacion o dispara
las balas que haran la comprobacion de colisiones para poder matar a los soldados
o destruir helicopteros.

Se ha añadido un sistema de dificultades en la que a cada minuto hay un 
aumento de la dificultad pues el tiempo de spawn de helicopteros y soldados
se reduce hasta 5 veces.

Para facilitar el dibujado de los elementos visuales, he creado una clase
VisualElement como haria unity con sus objetos de UI que contiene la información
de su posición, su tamaño, el sprite y una funcion de Draw virtual que cada objeto
como Soldier, Helicopter y Bullet tienen que implementar. Esto permite realizar modificiones
sobre los objetos visuales a modo de herencia que extenderan las clases que realmente vayan
a dibujar en pantalla. 

Por lo que hace referencia al resto de funcionalidades como los textos
simplemente se han añadido draw text en cada screen que los ha necesitado
con los textos expuestos en el enunciado.

///////////////////////////////////////////////////////////////////
			3. Citaciones
///////////////////////////////////////////////////////////////////

	Todas las imágenes usadas en el juego son las provistas por
los recursos del aula o en el caso del Logo es una imagen extraída del
libro de Rol Dragones y Mazmorras de WizardsOfTheCoast.
En el caso de los sonidos, la música se ha substituido por una canción de la
bso de Stardust y el sonido de las balas es una modificación del sonido de 
salto de Super Mario Bros.

///////////////////////////////////////////////////////////////////
