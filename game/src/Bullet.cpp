
#include "Bullet.h"


Bullet::Bullet() {

}

void Bullet::Setup(Vector2 direction, Vector2 position) 
{
	m_Direction = direction;
	m_Rect.x = position.x;
	m_Rect.y = position.y;
	m_State = FLYING;
	m_BulletSpeed = 500;
}

void Bullet::Draw() 
{
	DrawRectangle(m_Rect.x, m_Rect.y, 3, 3, RAYWHITE);
}

void Bullet::Update()
{
	m_Rect.x = m_Rect.x + m_BulletSpeed * m_Direction.x * GetFrameTime();
	m_Rect.y = m_Rect.y + m_BulletSpeed * m_Direction.y * GetFrameTime();
	m_BulletTime = std::max(m_BulletTime - GetFrameTime(), (float)0);
	if (m_BulletTime == 0)
		m_State = LOST;
}

bool Bullet::CheckCollision(Rectangle rect)
{
	if (CheckCollisionRecs(m_Rect, rect) && m_State == FLYING)
	{
		m_State = IMPACTED;
		return true;
	}
	return false;
}
