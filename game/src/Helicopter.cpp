#include "Helicopter.h"
#include <iostream>

Helicopter::Helicopter() {
	Setup(OUTSIDE,2.5);
}

void Helicopter::Draw()
{
	DrawTexture(m_Sprite, m_Rect.x, m_Rect.y,RAYWHITE);
}

Vector2 Helicopter::GetPosition()
{
	return Vector2{ m_Rect.x,m_Rect.y };
}

void Helicopter::Setup(HeliState state, float maxTimer) 
{

	m_InitPosyRight = 20 + AssetManager::GetInstance()->m_Helicopter_Right.height;
	m_InitPosLeft = 0 - AssetManager::GetInstance()->m_Helicopter_Right.width;
	m_InitPosRight = GetScreenWidth();
	m_SpawnTimer = maxTimer - rand()/RAND_MAX;
	m_MaxTimer = maxTimer;
	m_State = state;
	
	if(state == LEFT)
	{
		m_Sprite = AssetManager::GetInstance()->m_Helicopter_Left;
		m_Rect.y = m_InitPosyRight;
		m_Rect.x = m_InitPosRight;
		m_Rect.width = AssetManager::GetInstance()->m_Helicopter_Left.width;
		m_Rect.height = AssetManager::GetInstance()->m_Helicopter_Left.height;

	}
	else if (state == RIGHT) 
	{
		m_Sprite = AssetManager::GetInstance()->m_Helicopter_Right;
		m_Rect.y = m_InitPosyLeft;
		m_Rect.x = m_InitPosLeft;
		m_Rect.width = AssetManager::GetInstance()->m_Helicopter_Right.width;
		m_Rect.height = AssetManager::GetInstance()->m_Helicopter_Right.height;
	}
}

void Helicopter::Update()
{
	// Handle Death Timer
	if (m_State == DEAD) 
	{
		m_DeathTimer -= GetFrameTime();
		m_DeathTimer = std::max((float)0, m_DeathTimer);
	}
	// Handle Movement or enter DEAD state
	else
	{
		if (m_State == LEFT)
			m_Rect.x = m_Rect.x - m_MovementSpeed * GetFrameTime();		
		else if(m_State ==  RIGHT)	
			m_Rect.x = m_Rect.x + m_MovementSpeed * GetFrameTime();

		if (IsOutside())
			m_State = DEAD;
	}
	// Check if ready to spawn
	if (m_SpawnTimer == 0) {
		m_Ready2Spawn = true;
		ResetTimer();
	}
	// Lower Spawn Timer
	m_SpawnTimer = std::max(m_SpawnTimer - GetFrameTime(),(float)0);
	
}

void Helicopter::ResetTimer() {
	m_SpawnTimer = m_MaxTimer - ((float)rand()/(RAND_MAX));
}

bool Helicopter::CanSpawn() 
{
	if (m_Rect.x + AssetManager::GetInstance()->m_Trooper.width <= GetScreenWidth()
		&& m_Rect.x >= 0 && m_SpawnTimer == 0)
		return true;
	return false;
}

void Helicopter::OnDeath()
{
	m_Sprite = AssetManager::GetInstance()->m_Dead;
	m_Rect.width = AssetManager::GetInstance()->m_Dead.width;
	m_Rect.height = AssetManager::GetInstance()->m_Dead.height;
	m_DeathTimer = 2;
}

bool Helicopter::IsOutside()
{
	if ((m_Rect.x + m_Rect.width) <= 0)
		return true;
	else if (m_Rect.x >= GetScreenWidth())
		return true;
	return false;
}
