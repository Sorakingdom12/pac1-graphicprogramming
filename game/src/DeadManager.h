#pragma once

#include "Helicopter.h"
#include "Soldier.h";
#include <vector>
using namespace std;


class DeadManager
{
public:

	vector<Helicopter> m_DeadHelicopters;
	vector<Soldier> m_DeadSoldiers;

	DeadManager();
	void Draw();
	void Update();
	void AddHelicopter(Helicopter helicopter);
	void AddSoldier(Soldier soldier);
};

