/**********************************************************************************************
*
*   raylib - Advance Game template
*
*   Gameplay Screen Functions Definitions (Init, Update, Draw, Unload)
*
*   Copyright (c) 2014-2022 Ramon Santamaria (@raysan5)
*
*   This software is provided "as-is", without any express or implied warranty. In no event
*   will the authors be held liable for any damages arising from the use of this software.
*
*   Permission is granted to anyone to use this software for any purpose, including commercial
*   applications, and to alter it and redistribute it freely, subject to the following restrictions:
*
*     1. The origin of this software must not be misrepresented; you must not claim that you
*     wrote the original software. If you use this software in a product, an acknowledgment
*     in the product documentation would be appreciated but is not required.
*
*     2. Altered source versions must be plainly marked as such, and must not be misrepresented
*     as being the original software.
*
*     3. This notice may not be removed or altered from any source distribution.
*
**********************************************************************************************/

#include "raylib.h"
#include "screens.h"
#include "stdio.h"
#include "VisualElement.h"
#include "Bullet.h"
#include <iostream>
//----------------------------------------------------------------------------------
// Module Variables Definition (local)
//----------------------------------------------------------------------------------
static int framesCounter = 0;
static int finishScreen = 0;
static int m_BulletWaste = 0;
static int m_Landed = 0;
static int m_Score = 0;
static bool m_EOG = false;
static bool m_MatchResult = false;
float m_TurretSpeed = 200;
float m_Angle = 0;
EnemyManager m_EnemyManager;
vector<Bullet> m_Bullets;
Sound m_BulletSound = { 0 };
float m_Secs = 0;
int m_Mins = 0;
//----------------------------------------------------------------------------------
// Gameplay Screen Functions Definition
//----------------------------------------------------------------------------------

// Gameplay Screen Initialization logic
void InitGameplayScreen()
{
    // TODO: Initialize GAMEPLAY screen variables here!
    framesCounter = 0;
    finishScreen = 0;
    m_Secs = 0;
    m_Mins = 0;
    m_Landed = 0;
    m_BulletWaste = 0;
    m_BulletSound = AssetManager::GetInstance()->m_BulletSound;
    m_EnemyManager = EnemyManager();
    m_EnemyManager.Setup();
    m_Bullets = vector<Bullet>();
    SetSoundVolume(m_BulletSound, 0.1f);
}
// Gameplay Screen Update logic
void UpdateGameplayScreen(void)
{
    m_Secs = m_Secs + GetFrameTime();;
    if (m_Secs >= 60) {
        m_Mins++;
        m_Secs -= 60;
    }
    Vector2 position = { float(GetScreenWidth() / 2), float(GetScreenHeight() - 0.9 * AssetManager::GetInstance()->m_TurretBase.height)};
    if (IsKeyDown(KEY_A))
    {
        m_Angle -= m_TurretSpeed * GetFrameTime();
        m_Angle = std::max((float)-80, m_Angle);
    }
    if (IsKeyDown(KEY_D))
    {
        m_Angle += m_TurretSpeed * GetFrameTime();
        m_Angle = std::min((float)80, m_Angle);
    }
    if (IsKeyPressed(KEY_SPACE)) 
    {
        m_BulletWaste += 1;
        ShootBullet(m_Angle, position, (float)AssetManager::GetInstance()->m_Turret.height);
    }
    for (int i = 0; i < m_Bullets.size(); ++i)
    {
        m_Bullets[i].Update();
    }

    m_EnemyManager.Update();
    m_EnemyManager.CheckCollisions(m_Bullets);
    
    // If reached End Of Game change to ENDING screen
    if (CheckEOGCondition())
    {
        finishScreen = 1;
        PlaySound(fxCoin);
    }
    m_Score = max(m_EnemyManager.m_Score - m_BulletWaste,0);
    m_Landed = m_EnemyManager.m_Landed;
}

void AddScore(int score) {
    m_Score += score;
}

// Gameplay Screen Draw logic
void DrawGameplayScreen(void)
{
    
    DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(), BLACK);

    m_EnemyManager.Draw();
    //DrawBlackRectangles();

    for each (Bullet bull in m_Bullets)
        bull.Draw();
    DrawTurret();
    DrawTurretBase();
    DrawScoreInfo();
}

void DrawTurret() 
{
    Vector2 position = {float (GetScreenWidth() / 2), float(GetScreenHeight() - 0.9 * AssetManager::GetInstance()->m_TurretBase.height) };

    DrawTexturePro(AssetManager::GetInstance()->m_Turret,
        Rectangle{0,0,(float)AssetManager::GetInstance()->m_Turret.width ,(float)AssetManager::GetInstance()->m_Turret.height }, // Texture Coordinates
        Rectangle{(float) position.x , (float)position.y ,//Position
                  (float)AssetManager::GetInstance()->m_Turret.width ,(float)AssetManager::GetInstance()->m_Turret.height }, // Size
        Vector2{ (float)AssetManager::GetInstance()->m_Turret.width/2,(float)AssetManager::GetInstance()->m_Turret.height }, // Pivot
        m_Angle, // Angle
        RAYWHITE); // Color
}

void ShootBullet(float angle, Vector2 initPosition, float turretHeight) 
{
    float angleRad = (angle - 90)* PI / 180;
    Vector2 direction = { cos(angleRad),sin(angleRad) };
    Vector2 spawnPosition = { initPosition.x + turretHeight * direction.x, initPosition.y + turretHeight * direction.y };
    Bullet bullet;
    bullet.Setup(direction, spawnPosition);
    m_Bullets.push_back(bullet);
    PlaySoundMulti(m_BulletSound);
}

void DrawTurretBase() 
{
    DrawTexture(AssetManager::GetInstance()->m_TurretBase, (GetScreenWidth() - AssetManager::GetInstance()->m_TurretBase.width)/ 2, GetScreenHeight()- AssetManager::GetInstance()->m_TurretBase.height, RAYWHITE);
}

void DrawScoreInfo() 
{
    char score[6];
    sprintf(score, "%d", m_Score);
    char landed[3];
    sprintf(landed,"%d", m_Landed);

    char time[6];
    if(m_Mins<10 && m_Secs<10)
        sprintf(time, "0%d:0%d", m_Mins,(int)floor(m_Secs));
    else if(m_Mins<10 && m_Secs>=10)
        sprintf(time, "0%d:%d", m_Mins,(int)floor(m_Secs));
    else if(m_Mins >= 10 && m_Secs >= 10)
        sprintf(time, "%d:%d", m_Mins,(int)floor(m_Secs));
    else
        sprintf(time, "%d:0%d", m_Mins,(int)floor(m_Secs));


    DrawText("SCORE:", GetScreenWidth() / 10, 10, 20, RAYWHITE);
    DrawText(score, 2 * GetScreenWidth() / 10, 10, 20, DARKPURPLE);
    DrawText("TIME:", 4 * GetScreenWidth() / 10, 10, 20, RAYWHITE);
    DrawText(time, 5 * GetScreenWidth() / 10, 10, 20, DARKPURPLE);
    DrawText("LANDED:", 7.9 * GetScreenWidth() / 10, 10, 20, RAYWHITE);
    DrawText(landed, 9 * GetScreenWidth() / 10, 10, 20, DARKPURPLE);
}

void DrawBlackRectangles() {
    DrawRectangle(0, 0, GetScreenWidth() / 10, GetScreenHeight(), BLACK); // Left Side Border
    DrawRectangle(9 * GetScreenWidth() / 10, 0, GetScreenWidth(), GetScreenHeight(), BLACK); // Right Side Border
}

// Gameplay Screen Unload logic
void UnloadGameplayScreen()
{
    // TODO: Unload GAMEPLAY screen variables here!
}

// Gameplay Screen should finish?
int FinishGameplayScreen()
{
    return finishScreen;
}

bool HasWonMatch() 
{
    return m_MatchResult;
}

bool CheckEOGCondition()
{
    if (m_Landed >= 7)
        return true;
    return false;
}