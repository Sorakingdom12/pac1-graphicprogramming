#pragma once

#include <string>
#include "raylib.h"
using namespace std;

class AssetManager
{
public:
	Texture2D m_Title;
	Texture2D m_Turret;
	Texture2D m_TurretBase;
	Texture2D m_Parachuter;
	Texture2D m_Trooper;
	Texture2D m_Dead;
	Texture2D m_Helicopter_Left;
	Texture2D m_Helicopter_Right;
	Sound m_BulletSound;
	AssetManager();

	static AssetManager* Instance;

	static AssetManager* GetInstance() {
		if (Instance == nullptr) {
			Instance = new AssetManager();
		}
		return Instance; 
	};
};

