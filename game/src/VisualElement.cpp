#include "VisualElement.h"

VisualElement::VisualElement() 
{
	m_Rect = { 0,0,1,1 };
	m_Pivot = {0.5,0.5};
	m_Sprite = Texture2D();
}

VisualElement::VisualElement(Texture2D sprite, Vector2 Position) 
{
	m_Sprite = sprite;
	m_Rect = Rectangle{ Position.x, Position.y, Position.x + m_Sprite.width, Position.y + m_Sprite.height };
	m_Pivot = Vector2{ 0.5f,0.5f };
}

#pragma region GETTERS

Texture2D VisualElement::GetSprite() 
{
	return m_Sprite;
}

Rectangle VisualElement::GetRect()
{
	return m_Rect;
}

Vector2 VisualElement::GetSize()
{
	return Vector2{ m_Rect.width, m_Rect.height };
}

#pragma endregion

#pragma region SETTERS

void VisualElement::SetSprite(Texture2D sprite)
{
	m_Sprite = sprite;
}

void VisualElement::SetPivot(Vector2 newPivot)
{
	m_Pivot = newPivot;
}

void VisualElement::SetRotation(Vector2 direction)
{
}

#pragma endregion
