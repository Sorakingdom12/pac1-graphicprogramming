#pragma once

#include "DeadManager.h"
#include "Bullet.h"
#include <vector>
using namespace std;



class EnemyManager
{
public:
	enum Difficulty {
		Easy,
		Intermediate,
		Hard,
		Extreme,
		Madness
	};

	vector<Helicopter> m_Helicopters;
	vector<Soldier> m_Soldiers;
	vector<Bullet> m_bullets;
	Difficulty m_CurrentDifficulty;
	DeadManager m_DeadManager;
	int m_Score = 0;
	int m_Landed = 0;
	float m_DifficultyTimer;
	float m_MaxHeliSpawnTimer = 1.75;
	float m_MaxSpawnTimer;
	float m_SpawnTimer;


	/// <summary>
	/// Method called to draw every element controlled by the manager
	/// </summary>
	void Draw();
	/// <summary>
	/// Method called to initialize the Enemy Manager
	/// </summary>
	void Setup();
	/// <summary>
	/// Method called to update the logistics of the controlled items
	/// </summary>
	void Update();
	/// <summary>
	/// Method that checks collisions with every bullet available
	/// </summary>
	/// <param name="bullets"></param>
	void CheckCollisions(vector<Bullet>& bullets);
	/// <summary>
	/// Spawns an helicopter either on the left or on the right randomly
	/// </summary>
	void SpawnHelicopter();
	/// <summary>
	/// Spawns a soldier in on the given helicopter;
	/// </summary>
	void SpawnSoldier(Vector2 position);
	/// <Summary>
	/// 
	/// <Summary>
	void CheckDifficulty();
};