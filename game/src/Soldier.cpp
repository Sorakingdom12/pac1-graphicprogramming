#include "Soldier.h"
#include <AssetManager.h>

Soldier::Soldier() {
	m_DeathTimer = 2;
	m_OpenChuteTimer = (0.2+(float)rand() / (RAND_MAX + 2));
	m_FallingSpeed = 60;
	m_State = Falling;
	m_Sprite = AssetManager::GetInstance()->m_Trooper;
	m_Rect.width = m_Sprite.width;
	m_Rect.height = m_Sprite.height;
}

void Soldier::Draw() 
{
	DrawTexture(m_Sprite, m_Rect.x, m_Rect.y, RAYWHITE);
}

void Soldier::Update()
{
	if (m_State == Falling) {
		m_OpenChuteTimer = std::max((m_OpenChuteTimer - GetFrameTime()), (float)0);
		if (m_OpenChuteTimer == 0) {
			m_Sprite = AssetManager::GetInstance()->m_Parachuter;
			m_FallingSpeed /= 2;
			m_Rect.width = m_Sprite.width;
			m_Rect.height = m_Sprite.height;
			m_State = Gliding;
		}
	}

	if (m_State == Dead)
	{
		m_DeathTimer = std::max(m_DeathTimer-GetFrameTime(),(float)0);
		m_Sprite = AssetManager::GetInstance()->m_Dead;
		m_Rect.width = m_Sprite.width;
		m_Rect.height = m_Sprite.height;
	}

	else if(m_State == Falling)
	{
		m_Rect.y = m_Rect.y + m_FallingSpeed * GetFrameTime();

	}

	else if(m_State == Gliding)
	{
		m_Rect.y = m_Rect.y + m_FallingSpeed * GetFrameTime();		
	}

	if (m_Rect.y + m_Rect.height >= GetScreenHeight())
	{
		m_Sprite = AssetManager::GetInstance()->m_Trooper;
		m_State = Landed;
	}
}

void Soldier::Setup(Vector2 position) 
{
	m_Sprite = AssetManager::GetInstance()->m_Trooper;
	m_Rect = Rectangle{ position.x, position.y, (float)m_Sprite.width, (float)m_Sprite.height };
}

void Soldier::OpenParachute() 
{
	m_State = Gliding;
	m_Sprite = AssetManager::GetInstance()->m_Parachuter;
	m_FallingSpeed /= 2;
}

void Soldier::OnDeath() 
{
	m_DeathTimer = 2;
}