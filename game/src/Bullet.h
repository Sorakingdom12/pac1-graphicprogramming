#pragma once

#include "VisualElement.h"
using namespace std;

class Bullet : public VisualElement
{
public:

	enum BulletState {
		FLYING,
		IMPACTED,
		LOST
	};
	
	BulletState m_State;
	Vector2 m_Direction;
	float m_BulletSpeed = 30;
	float m_BulletTime = 4;


	Bullet();
	void Draw();
	void Setup(Vector2 direction, Vector2 position);
	void Update();
	bool CheckCollision(Rectangle rect);
};

