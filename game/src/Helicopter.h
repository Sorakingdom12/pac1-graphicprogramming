#pragma once
#include "AssetManager.h"
#include "VisualElement.h"
using namespace std;

class Helicopter :public VisualElement
{

public:

	enum HeliState
	{
		RIGHT,
		LEFT,
		OUTSIDE,
		DEAD
	};

	HeliState m_State;
	float m_DeathTimer = 1;
	float m_SpawnTimer = 3;
	float m_MaxTimer = 3;
	float m_MovementSpeed = 100;
	int m_InitPosyLeft = 20;
	int m_InitPosyRight ;
	int m_InitPosLeft ;
	int m_InitPosRight ;

	bool m_Ready2Spawn = false;

	Helicopter();

	Vector2 GetPosition();
	
	void Draw();
	void Setup(HeliState state, float maxTimer);
	void Update();
	bool CanSpawn();
	void ResetTimer();
	void OnDeath();
	bool IsOutside();
};