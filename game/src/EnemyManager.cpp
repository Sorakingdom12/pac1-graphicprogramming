#include "EnemyManager.h"
#include <iostream>


void EnemyManager::Setup() {
	m_Helicopters = vector<Helicopter>();
	m_Soldiers = vector<Soldier>();
	m_CurrentDifficulty = Easy;
	m_DifficultyTimer = 60;
	m_MaxSpawnTimer = 3;
	m_MaxHeliSpawnTimer = 2.75;
	m_SpawnTimer = m_MaxSpawnTimer;
}

void EnemyManager::Update() {

	m_DifficultyTimer = std::max(m_DifficultyTimer - GetFrameTime(), (float)0);
	// Update Dificulty
	CheckDifficulty();
	m_SpawnTimer = std::max(m_SpawnTimer-GetFrameTime(),(float)0);
	// Check if can Spawn Helicopter
	if (m_SpawnTimer == 0)
	{
		SpawnHelicopter();
		m_SpawnTimer = m_MaxSpawnTimer + (rand()/(RAND_MAX));
	}
	//Check Helicopter Logistics and move them
	for (int i = 0; i < m_Helicopters.size(); ++i)
	{
		if (m_Helicopters[i].m_State == Helicopter::DEAD)
		{
			Helicopter heli = m_Helicopters[i]; 
			heli.OnDeath();
			m_Helicopters.erase(m_Helicopters.begin()+i);
			m_DeadManager.AddHelicopter(heli);
			--i;
		}
		else {
			if (m_Helicopters[i].CanSpawn()) {
				m_Helicopters[i].m_Ready2Spawn = false;
				SpawnSoldier(m_Helicopters[i].GetPosition());
			}
			m_Helicopters[i].Update();
		}
	}
	//Check Soldier Logistics and move them
	int counter = 0;
	for (int j = 0; j < m_Soldiers.size(); ++j) {
		if (m_Soldiers[j].m_State == Soldier::Landed)
			counter++;
		if (m_Soldiers[j].m_State == Soldier::Dead)
		{
			Soldier sold = m_Soldiers[j];
			sold.OnDeath();
			m_Soldiers.erase(m_Soldiers.begin() + j);
			m_DeadManager.AddSoldier(sold);
			--j;
		}
		else m_Soldiers[j].Update();
		
	}	
	m_Landed = counter;
	m_DeadManager.Update();
}

void EnemyManager::CheckCollisions(vector<Bullet>& bullets)
{
	for (int j = 0; j < bullets.size(); ++j)
	{
		for (int i = 0; i < m_Helicopters.size(); ++i)
		{
			if (bullets[j].CheckCollision(m_Helicopters[i].GetRect()))
			{
				m_Helicopters[i].m_State = Helicopter::DEAD;
				bullets[j].m_State = Bullet::IMPACTED;
				m_Score += 10;
				break;
			}
		}

		if (bullets[j].m_State != Bullet::IMPACTED)
		{
			for (int i = 0; i <m_Soldiers.size(); ++i)
			{
				if (bullets[j].CheckCollision(m_Soldiers[i].GetRect()))
				{
					m_Soldiers[i].m_State = Soldier::Dead;
					bullets[j].m_State = Bullet::IMPACTED;
					m_Score += 5;
					break;
				}
			}
		}
	}

	for (int i = 0; i < bullets.size(); ++i)
	{
		if(bullets[i].m_State == Bullet::IMPACTED || bullets[i].m_State == Bullet::LOST)
		{
			bullets.erase(bullets.begin() + i);
			--i;
		}
	}
}

void EnemyManager::Draw() {

	for (int i = 0; i < m_Helicopters.size(); ++i)
		m_Helicopters[i].Draw();

	for (int j = 0; j < m_Soldiers.size(); ++j)
		m_Soldiers[j].Draw();
	m_DeadManager.Draw();
}

void EnemyManager::CheckDifficulty() {
	if (m_DifficultyTimer == 0)
	{
		m_DifficultyTimer = 60;
		switch (m_CurrentDifficulty)
		{
			case Easy:
				m_CurrentDifficulty = Intermediate;
				m_MaxHeliSpawnTimer = 2.75;
				m_MaxSpawnTimer = 2.5;
			case Intermediate:
				m_CurrentDifficulty = Hard;
				m_MaxHeliSpawnTimer = 2.25;
				m_MaxSpawnTimer = 2.0;

			case Hard:
				m_CurrentDifficulty = Extreme;
				m_MaxHeliSpawnTimer = 2;
				m_MaxSpawnTimer = 1.75;

			case Extreme:
				m_CurrentDifficulty = Madness;
				m_MaxHeliSpawnTimer = 1.5;
				m_MaxSpawnTimer = 1.5;
		}
	}
}

void EnemyManager::SpawnHelicopter() {
	Helicopter newHelicopter;
	
	float random = (float)rand()/RAND_MAX;
	if (random <= 0.5) {
		(newHelicopter).Setup(Helicopter::LEFT, m_MaxHeliSpawnTimer);
	}
	else {
		(newHelicopter).Setup(Helicopter::RIGHT, m_MaxHeliSpawnTimer);
	}
	m_Helicopters.push_back(newHelicopter);
}

void EnemyManager::SpawnSoldier(Vector2 position) {
	Soldier sold;
	sold.Setup(position);
	m_Soldiers.push_back(sold);
}
