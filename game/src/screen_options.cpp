/**********************************************************************************************
*
*   raylib - Advance Game template
*
*   Options Screen Functions Definitions (Init, Update, Draw, Unload)
*
*   Copyright (c) 2014-2022 Ramon Santamaria (@raysan5)
*
*   This software is provided "as-is", without any express or implied warranty. In no event
*   will the authors be held liable for any damages arising from the use of this software.
*
*   Permission is granted to anyone to use this software for any purpose, including commercial
*   applications, and to alter it and redistribute it freely, subject to the following restrictions:
*
*     1. The origin of this software must not be misrepresented; you must not claim that you
*     wrote the original software. If you use this software in a product, an acknowledgment
*     in the product documentation would be appreciated but is not required.
*
*     2. Altered source versions must be plainly marked as such, and must not be misrepresented
*     as being the original software.
*
*     3. This notice may not be removed or altered from any source distribution.
*
**********************************************************************************************/

#include "raylib.h"
#include "screens.h"

//----------------------------------------------------------------------------------
// Module Variables Definition (local)
//----------------------------------------------------------------------------------
static int framesCounter = 0;
static int finishScreen = 0;

//----------------------------------------------------------------------------------
// Options Screen Functions Definition
//----------------------------------------------------------------------------------

// Options Screen Initialization logic
void InitOptionsScreen(void)
{
    // TODO: Initialize OPTIONS screen variables here!
    framesCounter = 0;
    finishScreen = 0;
}

// Options Screen Update logic
void UpdateOptionsScreen(void)
{
    // TODO: Update OPTIONS screen variables here!
    if (IsKeyPressed(KEY_O))
        finishScreen = 1;
}

// Options Screen Draw logic
void DrawOptionsScreen(void)
{
    DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(), BLACK);
    DrawText("    Do not allow enemy paratroopers to land     ", 2*GetScreenWidth()/10,100 , 20, RAYWHITE);
    DrawText("    on either side of your gun base. If seven   ", 2*GetScreenWidth()/10,120 , 20, RAYWHITE);
    DrawText("   paratroopers land on your base you'll lose   ", 2*GetScreenWidth()/10,140 , 20, RAYWHITE);



    DrawText("             Press 'SPACE' to shoot.            ", 2*GetScreenWidth()/10,240 , 20, RAYWHITE);
    DrawText(" Press 'A' to move the turret counterclockwise. ", 2*GetScreenWidth()/10,260 , 20, RAYWHITE);
    DrawText("     Press 'D' to move the turret clockwise.    ", 2*GetScreenWidth()/10,280 , 20, RAYWHITE);
    DrawText("          Press 'O' to return to Title.         ", 2*GetScreenWidth()/10,320 , 20, RAYWHITE);
}

// Options Screen Unload logic
void UnloadOptionsScreen(void)
{
    // TODO: Unload OPTIONS screen variables here!
}

// Options Screen should finish?
int FinishOptionsScreen(void)
{
    return finishScreen;
}