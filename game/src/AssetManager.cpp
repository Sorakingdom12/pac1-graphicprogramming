#include "AssetManager.h"

AssetManager::AssetManager() {
	m_Parachuter = LoadTexture("resources/Enemies/Parachuter.png");
	m_Title = LoadTexture("resources/Menus/Title.png");
	m_Turret = LoadTexture("resources/Player/Turret.png");
	m_TurretBase = LoadTexture("resources/Player/PlayerBody.png");
	m_Trooper = LoadTexture("resources/Enemies/Trooper.png");
	m_Dead = LoadTexture("resources/Enemies/Dead.png");
	m_Helicopter_Left = LoadTexture("resources/Enemies/Helicopter_Right.png");
	m_Helicopter_Right = LoadTexture("resources/Enemies/Helicopter_Left.png");
	m_BulletSound = LoadSound("resources/bullet_shot.wav");
}

AssetManager* AssetManager::Instance;