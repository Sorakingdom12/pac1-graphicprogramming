#pragma once

#include "VisualElement.h"
using namespace std;

class Soldier :public VisualElement
{
public:

	enum SolState {
		Falling,
		Gliding,
		Landed,
		Dead
	};

#pragma region Variables

	SolState m_State = Falling;
	float m_FallingSpeed = 40;
	float m_OpenChuteTimer = 1;
	float m_DeathTimer = 1;

#pragma endregion

#pragma region Methods
	Soldier();
	void Draw();
	void Update();
	void Setup(Vector2 position);
	void OpenParachute();
	void OnDeath();
#pragma endregion
	
};

