#include "DeadManager.h"

DeadManager::DeadManager() {

}

void DeadManager::AddHelicopter(Helicopter helicopter)
{
	m_DeadHelicopters.insert(m_DeadHelicopters.begin(), helicopter);
}

void DeadManager::AddSoldier(Soldier soldier)
{
	m_DeadSoldiers.insert(m_DeadSoldiers.begin(), soldier);
}

void DeadManager::Update() {
	for (int i = 0; i < m_DeadHelicopters.size(); ++i) 
	{
		m_DeadHelicopters[i].Update();
		if (m_DeadHelicopters[i].m_DeathTimer == 0)
		{
			m_DeadHelicopters.erase(m_DeadHelicopters.begin() + i);
		}
	}
	for (int i = 0; i < m_DeadSoldiers.size(); ++i)
	{
		
		m_DeadSoldiers[i].Update();
		if (m_DeadSoldiers[i].m_DeathTimer == 0)
		{
			m_DeadSoldiers.erase(m_DeadSoldiers.begin() + i);
		}
	}
}

void DeadManager::Draw() {
	for (int i = 0; i < m_DeadHelicopters.size(); ++i)
	{
		m_DeadHelicopters[i].Draw();
	}
	for (int i = 0; i < m_DeadSoldiers.size(); ++i)
	{
		m_DeadSoldiers[i].Draw();
	}
}